import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Log } from './log.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  calcForm: FormGroup;

  operator: string;
  operators: string[];

  calculationLog: Log[] = [];

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.operators = ['+', '-', '*', '/', 'mod', '^'];

    this.calcForm = this.formBuilder.group({
      'lo': '',
      'ro': '',
      'o': ''
    });
  }

  calculate() {
    let left = Number(this.calcForm.get('lo').value);
    let right = Number(this.calcForm.get('ro').value);

    if (left === undefined || right === undefined) {
      console.log("Please, enter value");
      return;
    }
    if (this.operator === undefined) {
      console.log("Please, select operation");
      return;
    }

    let log = {
      'right': right,
      'op': this.operator,
      'left': left,
      'result': undefined
    };

    switch (this.operator) {
      case '+': {
        log.result = left + right;
        break;
      }
      case '-': {
        log.result = left - right;
        break;
      }
      case '*': {
        log.result = left * right;
        break;
      }
      case '/': {
        log.result = left / right;
        break;
      }
      case 'mod': {
        log.result = left % right;
        break;
      }
      case '^': {
        log.result = Math.pow(left, right);
        break;
      }
    }
    
    this.calculationLog.push(log);
  }

}
