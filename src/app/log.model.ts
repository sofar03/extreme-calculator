export interface Log {

    right: number;
    op: string;
    left: number;
    result: number;

}